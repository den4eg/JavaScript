"use strict";

const DATA = [
  {
    "first name": "Олексій",
    "last name": "Петров",
    photo: "./img/trainers/trainer-m1.jpg",
    specialization: "Басейн",
    category: "майстер",
    experience: "8 років",
    description:
      "Олексій має багаторічний досвід роботи з плавцями. Він займається якісною підготовкою спортсменів на міжнародних змаганнях. Його методика базується на новітніх технологіях тренувань.",
  },
  {
    "first name": "Марина",
    "last name": "Іванова",
    photo: "./img/trainers/trainer-f1.png",
    specialization: "Тренажерний зал",
    category: "спеціаліст",
    experience: "2 роки",
    description:
      "Марина спеціалізується на роботі з ваговими тренажерами. Вона розробила унікальну програму для набору м'язової маси. Її клієнти завжди задоволені результатами.",
  },
  {
    "first name": "Ігор",
    "last name": "Сидоренко",
    photo: "./img/trainers/trainer-m2.jpg",
    specialization: "Дитячий клуб",
    category: "інструктор",
    experience: "1 рік",
    description:
      "Ігор працює з дітьми різного віку. Він створив ігрові методики для розвитку координації та спритності. Його уроки завжди цікаві та корисні для малюків.",
  },
  {
    "first name": "Тетяна",
    "last name": "Мороз",
    photo: "./img/trainers/trainer-f2.jpg",
    specialization: "Бійцівський клуб",
    category: "майстер",
    experience: "10 років",
    description:
      "Тетяна є експертом в бойових мистецтвах. Вона проводить тренування для професіоналів і новачків. Її підхід до навчання допомагає спортсменам досягати високих результатів.",
  },
  {
    "first name": "Сергій",
    "last name": "Коваленко",
    photo: "./img/trainers/trainer-m3.jpg",
    specialization: "Тренажерний зал",
    category: "інструктор",
    experience: "1 рік",
    description:
      "Сергій фокусується на роботі з фітнесом та кардіотренуваннями. Він вдосконалив свої методики протягом багатьох років. Його учні завжди в формі та енергійні.",
  },
  {
    "first name": "Олена",
    "last name": "Лисенко",
    photo: "./img/trainers/trainer-f3.jpg",
    specialization: "Басейн",
    category: "спеціаліст",
    experience: "4 роки",
    description:
      "Олена спеціалізується на синхронному плаванні. Вона тренує команди різного рівня. Її команди завжди займають призові місця на змаганнях.",
  },
  {
    "first name": "Андрій",
    "last name": "Волков",
    photo: "./img/trainers/trainer-m4.jpg",
    specialization: "Бійцівський клуб",
    category: "інструктор",
    experience: "1 рік",
    description:
      "Андрій має досвід у вивченні різних бойових мистецтв. Він викладає техніку та тактику бою. Його учні здобувають перемоги на міжнародних турнірах.",
  },
  {
    "first name": "Наталія",
    "last name": "Романенко",
    photo: "./img/trainers/trainer-f4.jpg",
    specialization: "Дитячий клуб",
    category: "спеціаліст",
    experience: "3 роки",
    description:
      "Наталія розробила унікальну програму для дітей дошкільного віку. Вона допомагає дітям розвивати фізичні та ментальні навички. Її класи завжди веселі та динамічні.",
  },
  {
    "first name": "Віталій",
    "last name": "Козлов",
    photo: "./img/trainers/trainer-m5.jpg",
    specialization: "Тренажерний зал",
    category: "майстер",
    experience: "10 років",
    description:
      "Віталій спеціалізується на функціональному тренуванні. Він розробив ряд ефективних тренувальних програм. Його клієнти швидко досягають бажаних результатів.",
  },
  {
    "first name": "Юлія",
    "last name": "Кравченко",
    photo: "./img/trainers/trainer-f5.jpg",
    specialization: "Басейн",
    category: "спеціаліст",
    experience: "4 роки",
    description:
      "Юлія є експертом у водних видах спорту. Вона проводить тренування з аквагімнастики та аеробіки. Її учні демонструють вражаючі показники на змаганнях.",
  },
  {
    "first name": "Олег",
    "last name": "Мельник",
    photo: "./img/trainers/trainer-m6.jpg",
    specialization: "Бійцівський клуб",
    category: "майстер",
    experience: "12 років",
    description:
      "Олег є визнаним майстром в бойових мистецтвах. Він тренує чемпіонів різних вагових категорій. Його методики вважаються одними з найефективніших у світі бойових мистецтв.",
  },
  {
    "first name": "Лідія",
    "last name": "Попова",
    photo: "./img/trainers/trainer-f6.jpg",
    specialization: "Дитячий клуб",
    category: "інструктор",
    experience: "1 рік",
    description:
      "Лідія має великий досвід у роботі з дітьми. Вона організовує різноманітні спортивні ігри та заняття. Її класи завжди допомагають дітям розвивати соціальні навички та командний дух.",
  },
  {
    "first name": "Роман",
    "last name": "Семенов",
    photo: "./img/trainers/trainer-m7.jpg",
    specialization: "Тренажерний зал",
    category: "спеціаліст",
    experience: "2 роки",
    description:
      "Роман є експертом у кросфіту та функціональних тренуваннях. Він розробив власні програми для різних вікових груп. Його учні часто отримують нагороди на фітнес-турнірах.",
  },
  {
    "first name": "Анастасія",
    "last name": "Гончарова",
    photo: "./img/trainers/trainer-f7.jpg",
    specialization: "Басейн",
    category: "інструктор",
    experience: "1 рік",
    description:
      "Анастасія фокусується на водних програмах для здоров'я та фітнесу. Вона проводить тренування для осіб з різним рівнем підготовки. Її учні відзначають покращення здоров'я та благополуччя після занять.",
  },
  {
    "first name": "Валентин",
    "last name": "Ткаченко",
    photo: "./img/trainers/trainer-m8.jpg",
    specialization: "Бійцівський клуб",
    category: "спеціаліст",
    experience: "2 роки",
    description:
      "Валентин є експертом в таеквондо та кікбоксингу. Він викладає техніку, тактику та стратегію бою. Його учні часто стають чемпіонами на національних та міжнародних змаганнях.",
  },
  {
    "first name": "Лариса",
    "last name": "Петренко",
    photo: "./img/trainers/trainer-f8.jpg",
    specialization: "Дитячий клуб",
    category: "майстер",
    experience: "7 років",
    description:
      "Лариса розробила комплексну програму для розвитку фізичних та інтелектуальних навичок дітей. Вона проводить заняття в ігровій формі. Її методика допомагає дітям стати активними та розумними.",
  },
  {
    "first name": "Олексій",
    "last name": "Петров",
    photo: "./img/trainers/trainer-m9.jpg",
    specialization: "Басейн",
    category: "майстер",
    experience: "11 років",
    description:
      "Олексій має багаторічний досвід роботи з плавцями. Він займається якісною підготовкою спортсменів на міжнародних змаганнях. Його методика базується на новітніх технологіях тренувань.",
  },
  {
    "first name": "Марина",
    "last name": "Іванова",
    photo: "./img/trainers/trainer-f9.jpg",
    specialization: "Тренажерний зал",
    category: "спеціаліст",
    experience: "2 роки",
    description:
      "Марина спеціалізується на роботі з ваговими тренажерами. Вона розробила унікальну програму для набору м'язової маси. Її клієнти завжди задоволені результатами.",
  },
  {
    "first name": "Ігор",
    "last name": "Сидоренко",
    photo: "./img/trainers/trainer-m10.jpg",
    specialization: "Дитячий клуб",
    category: "інструктор",
    experience: "1 рік",
    description:
      "Ігор працює з дітьми різного віку. Він створив ігрові методики для розвитку координації та спритності. Його уроки завжди цікаві та корисні для малюків.",
  },
  {
    "first name": "Наталія",
    "last name": "Бондаренко",
    photo: "./img/trainers/trainer-f10.jpg",
    specialization: "Бійцівський клуб",
    category: "майстер",
    experience: "8 років",
    description:
      "Наталія є майстром у бойових мистецтвах. Вона вивчала різні техніки та стили, із якими працює зі своїми учнями. Її підхід до навчання відповідає найвищим стандартам.",
  },
  {
    "first name": "Андрій",
    "last name": "Семенов",
    photo: "./img/trainers/trainer-m11.jpg",
    specialization: "Тренажерний зал",
    category: "інструктор",
    experience: "1 рік",
    description:
      "Андрій спеціалізується на функціональному тренуванні. Він розробив власну систему вправ для зміцнення корпусу. Його учні завжди отримують видимі результати.",
  },
  {
    "first name": "Софія",
    "last name": "Мельник",
    photo: "./img/trainers/trainer-f11.jpg",
    specialization: "Басейн",
    category: "спеціаліст",
    experience: "6 років",
    description:
      "Софія працює з аквагімнастикою. Вона вивчила різні техніки та стили плавання. Її заняття допомагають клієнтам розслабитися та покращити фізичну форму.",
  },
  {
    "first name": "Дмитро",
    "last name": "Ковальчук",
    photo: "./img/trainers/trainer-m12.png",
    specialization: "Дитячий клуб",
    category: "майстер",
    experience: "10 років",
    description:
      "Дмитро спеціалізується на розвитку дитячого спорту. Він розробив унікальну програму для малюків. Його методики забезпечують гармонійний розвиток дітей.",
  },
  {
    "first name": "Олена",
    "last name": "Ткаченко",
    photo: "./img/trainers/trainer-f12.jpg",
    specialization: "Бійцівський клуб",
    category: "спеціаліст",
    experience: "5 років",
    description:
      "Олена є відомим тренером у жіночому бойовому клубі. Вона вивчила різні техніки самооборони. Її підхід дозволяє її ученицям відчувати себе впевнено в будь-яких ситуаціях.",
  },
];




// ---------------------------------------------------------------------------------------------------------




// Створюємо новий елемент з класом "loader"
const loaderDiv = document.createElement("div");
loaderDiv.classList.add("loader");

// Знаходимо елемент з класом "header"
const headerElement = document.querySelector("header");

// Вставляємо новий елемент після елемента з класом "header"
headerElement.parentNode.insertBefore(loaderDiv, headerElement.nextSibling);

// Видаляємо елемент через 3 секунд
setTimeout(function () {
  loaderDiv.parentNode.removeChild(loaderDiv);
}, 3000);

function removeHiddenFromAll() {
  const hiddenElements = document.querySelectorAll(
    "section.sorting[hidden], aside.sidebar[hidden]"
  );

  hiddenElements.forEach(function (element) {
    element.removeAttribute("hidden");
  });
}

// Запускаємо функцію видалення після 3 секунд після завантаження сторінки
setTimeout(removeHiddenFromAll, 3000);

// Дані про тренерів (змінна DATA)

// Функція для створення картки тренера
function createTrainerCard(trainer) {
  const card = document.createElement("li");
  card.classList.add("trainer");

  const photo = document.createElement("img");
  photo.classList.add("trainer__img");
  photo.src = trainer.photo;
  photo.alt = "Trainer Photo";
  card.appendChild(photo);

  const name = document.createElement("h2");
  name.classList.add("trainer__name");
  name.textContent = `${trainer["first name"]} ${trainer["last name"]}`;
  card.appendChild(name);

  const trainerBtn = document.createElement("button");
  trainerBtn.classList.add("trainer__show-more");
  trainerBtn.textContent = "ПОКАЗАТИ";
  card.appendChild(trainerBtn);

  trainerBtn.addEventListener("click", (event) => {
    const modalWindow = document.createElement("div");
    modalWindow.classList.add("modal");

    modalWindow.innerHTML = `
      <div class="modal__body">
        <img src="${trainer.photo}" alt="" class="modal__img" width="280" height="360"/>
        <div class="modal__description">
          <p class="modal__name">${trainer["last name"]} ${trainer["first name"]}</p>
          <p class="modal__point modal__point--category">Категорія: ${trainer.category}</p>
          <p class="modal__point modal__point--experience">Досвід: ${trainer.experience}</p>
          <p class="modal__point modal__point--specialization">Напрям тренера: ${trainer.specialization}</p>
          <p class="modal__text">${trainer.description}</p>
        </div>
        <button class="modal__close">
          <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 72 72">
            <path d="M 19 15 C 17.977 15 16.951875 15.390875 16.171875 16.171875 C 14.609875 17.733875 14.609875 20.266125 16.171875 21.828125 L 30.34375 36 L 16.171875 50.171875 C 14.609875 51.733875 14.609875 54.266125 16.171875 55.828125 C 16.951875 56.608125 17.977 57 19 57 C 20.023 57 21.048125 56.609125 21.828125 55.828125 L 36 41.65625 L 50.171875 55.828125 C 51.731875 57.390125 54.267125 57.390125 55.828125 55.828125 C 57.391125 54.265125 57.391125 51.734875 55.828125 50.171875 L 41.65625 36 L 55.828125 21.828125 C 57.390125 20.266125 57.390125 17.733875 55.828125 16.171875 C 54.268125 14.610875 51.731875 14.609875 50.171875 16.171875 L 36 30.34375 L 21.828125 16.171875 C 21.048125 15.391875 20.023 15 19 15 z"></path>
          </svg>
        </button>
      </div>`;

    document.body.append(modalWindow);
    disableScroll();

    const buttonClose = modalWindow.querySelector(".modal__close");
    buttonClose.addEventListener("click", (event) => {
      modalWindow.remove();
      enableScroll();
    });
  });

  return card;
}

// Функція сортування тренерів
function sortTrainers(data, sortingType) {
  switch (sortingType) {
    case "ЗА замовчуванням":
      return data.sort(() => Math.random() - 0.5);
    case "ЗА ПРІЗВИЩЕМ":
      return data.sort((a, b) => a["last name"].localeCompare(b["last name"]));
    case "ЗА ДОСВІДОМ":
      return data.sort((a, b) => parseInt(a.experience) - parseInt(b.experience));
    default:
      return data;
  }
}

// Фільтрація по напрямку
function filterDataByDirection(value, data) {
  switch (value) {
    case "all-direction":
      return data;
    case "gym":
      return data.filter((elem) => elem.specialization === "Тренажерний зал");
    case "fight-club":
      return data.filter((elem) => elem.specialization === "Бійцівський клуб");
    case "kids-club":
      return data.filter((elem) => elem.specialization === "Дитячий клуб");
    case "swimming-pool":
      return data.filter((elem) => elem.specialization === "Басейн");
    default:
      return data;
  }
}

// Фільтрація по категорії
function filterDataByCategory(value, data) {
  switch (value) {
    case "all-category":
      return data;
    case "master":
      return data.filter((elem) => elem.category === "майстер");
    case "specialist":
      return data.filter((elem) => elem.category === "спеціаліст");
    case "instructor":
      return data.filter((elem) => elem.category === "інструктор");
    default:
      return data;
  }
}

// Функція фільтрації та сортування тренерів
function filterAndSortTrainers() {
  const direction = document.querySelector('input[name="direction"]:checked').id;
  const category = document.querySelector('input[name="category"]:checked').id;
  const sortingType = document.querySelector('.sorting__btn--active').textContent.trim();

  let filteredData = filterDataByDirection(direction, DATA);
  filteredData = filterDataByCategory(category, filteredData);
  const sortedData = sortTrainers(filteredData, sortingType);

  displayTrainers(sortedData);
}

// Функція для виведення карток тренерів на сторінку
function displayTrainers(data) {
  const trainersContainer = document.querySelector(".trainers-cards__container");
  trainersContainer.innerHTML = "";

  data.forEach((trainer) => {
    const card = createTrainerCard(trainer);
    trainersContainer.appendChild(card);
  });
}


// Початкове відображення всіх тренерів при завантаженні сторінки
// document.addEventListener("DOMContentLoaded", () => {
//   filterAndSortTrainers();
// });


// Отримуємо всі кнопки сортування
const sortingButtons = document.querySelectorAll(".sorting__btn");

// Додаємо обробник подій для кожної кнопки сортування
sortingButtons.forEach((button) => {
  button.addEventListener("click", function () {
    sortingButtons.forEach((btn) => btn.classList.remove("sorting__btn--active"));
    button.classList.add("sorting__btn--active");

    filterAndSortTrainers();
  });
});

// Отримуємо форму фільтрації
const filterForm = document.querySelector(".sidebar__filters");

// Обробник подій для форми фільтрації
filterForm.addEventListener("submit", (event) => {
  event.preventDefault();
  filterAndSortTrainers();
});


// Начальное отображение всех тренеров с задержкой 3 секунды при загрузке страницы
document.addEventListener("DOMContentLoaded", () => {
  setTimeout(filterAndSortTrainers, 3000);
});


// ------------------------------------------------------------------------------------

let scrollPosition = null;

const disableScroll = () => {
  scrollPosition = window.scrollY;
  document.body.style.width = "100%";
  document.body.style.overflow = "hidden";
  document.body.style.position = "fixed";
  document.body.style.top = `-${scrollPosition}px`;
};

const enableScroll = () => {
  document.body.style.width = "";
  document.body.style.overflow = "";
  document.body.style.position = "";
  document.body.style.top = "";
  window.scrollTo(0, scrollPosition);
};








