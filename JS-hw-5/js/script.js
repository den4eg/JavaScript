/*  
Теоретичні питання

1. Як можна створити функцію та як ми можемо її викликати?

2. Що таке оператор return в JavaScript? Як його використовувати в функціях?

3. Що таке параметри та аргументи в функіях та в яких випадках вони використовуються?

4. Як передати функцію аргументом в іншу функцію?

---------------------------------------------------------------------------
1.Існує кілька способів визначення функцій. Найбільш поширені - function declaration, function expression.
 Function declaration (Оголошення функцій) визначає ім'я функції за допомогою ключового слова `function`.
  Зазвичай вони піднімаються (hoisted), що означає, що вони доступні в усій області, 
 в якій вони визначені, навіть якщо вони оголошені після їх використання.

  Function Expression передбачає визначення функції та присвоювання її змінній. 
  Ці функції можуть бути анонімними або мати ім'я.
  Вони не піднімаються (не доступні до виклику) і можуть бути використані лише після того, як їх призначено.

2.Оператор повернення(return): 
   Оператори повернення дозволяють функціям передавати дані коду,
 що їх викликав, сприяючи передачі обчислених значень, результатів або будь-якої іншої інформації,
 необхідної для подальшої обробки чи маніпуляцій. 
   Розуміння операторів повернення є фундаментальним у JavaScript,
 оскільки вони впливають на поведінку та результати функцій.
     
   Використовується для завершення виконання функції та вказує значення, яке повертається викликачеві.

3.  Параметри - це місцедержателі, вказані в оголошенні функції. 
   Вони представляють значення, які функція очікує отримати під час виклику.
   Аргументи - це фактичні значення, що подаються до функції при її виклику. 
   Вони замінюють параметри всередині функції.
   Розуміння різниці між параметрами та аргументами функцій має важливе значення при роботі з функціями JavaScript.
   Параметри є місцедержателями в оголошенні функції, а аргументи - це значення,
 що передаються у функцію під час її виклику та замінюють параметри для виконання функції.
   Ці концепції є фундаментальними для передачі даних у функції та визначення її поведінки,
 на основі наданих значень.
 ноді терміни "аргумент" і "параметр" можуть використовуватися взаємозамінно. Проте, за визначенням,
 параметри - це те, що ви вказуєте в оголошенні функції, 
 у той час як аргументи - це те, що ви передаєте у функцію.

4. Callback functions (Функції зворотнього виклику в JavaScript - це функції, 
 які передаються як аргументи іншим функціям для виконання в майбутньому.)
  Їх часто використовують у сценаріях, де операція є асинхронною або потребує чекати на виникнення події.
  Будь-яка оголошена функція може бути передана в іншу функція як аргумент. 
  Оскільки, кожна функція є об’єктом, то передається посилання на цю функцію. 
  Функція, яка отримує посилання може за цим посиланням викликати іншу функцію дотримуючись правильного 
 задавання кількості та типу параметрів.
*/

function calculator() {

let isRepeat;
alert('Простий калькулятор')
do {
  

  let operation = prompt("Виберіть математичну операцію : +, -, *, /");

  let firstNumber = prompt("Будь ласка, введіть перше число");
  let secondNumber = prompt("Будь ласка, введіть друге число");

  firstNumber = parseFloat(firstNumber);
  secondNumber = parseFloat(secondNumber);

  let result;

  switch (operation) {
    case "+":
      result = firstNumber + secondNumber;
      break;
    case "-":
      result = firstNumber - secondNumber;
      break;
    case "*":
      result = firstNumber * secondNumber;
      break;
    case "/":
      if (secondNumber === 0) {
        alert("Такої операції не має");
      } else {
        result = firstNumber / secondNumber;
      }
      break;
    default:
      alert("Такої операції не існує");
  }
  alert(`Результат ${result}`);
  console.log(result)
  isRepeat = prompt("Може ще разок?) (так/ні)").toLowerCase() === "так";
} while (isRepeat);
}
 calculator();