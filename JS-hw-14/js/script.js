/*
Теоритичні питання:
1. В чому полягає відмінність localStorage і sessionStorage?
2. Які аспекти безпеки слід враховувати при збереженні чутливої інформації, такої як паролі, за допомогою localStorage чи sessionStorage?
3. Що відбувається з даними, збереженими в sessionStorage, коли завершується сеанс браузера?

-----------------------------------------------------------------------------------
1.
LocalStorage і SessionStorage - це два типи веб-сховища, які дозволяють зберігати дані на браузері клієнта.
 Основна відмінність між ними полягає в тому, що дані, збережені в LocalStorage, залишаються на пристрої назавжди,
  поки їх не видалити вручну або не очистити кеш браузера, тоді як дані, збережені в SessionStorage, 
  будуть видалені після закриття вкладки або вікна браузера.

Крім того, дані в LocalStorage доступні на всіх вкладках і вікнах браузера, 
тоді як дані в SessionStorage доступні тільки в поточній вкладці або вікні.

2.
Шифрування: перед збереженням чутливої інформації у localStorage чи sessionStorage, рекомендується її шифрувати. 
Це допоможе захистити дані від несанкціонованого доступу.

Використання безпечних методів передачі: при передачі чутливої інформації з клієнта на сервер і навпаки, 
слід використовувати захищені протоколи передачі даних, такі як HTTPS.

Не зберігати паролі напряму: краще не зберігати паролі у відкритому вигляді у localStorage чи sessionStorage. 
Замість цього, можна зберігати хеш паролю, а потім порівнювати хеші під час автентифікації.

Використання механізмів безпеки браузера: браузери надають деякі механізми безпеки, такі як Content Security Policy (CSP) і SameSite cookies, 
які можуть допомогти у захисті даних, збережених у localStorage чи sessionStorage.

Регулярна перевірка та очищення даних: рекомендується періодично перевіряти та очищати дані, які зберігаються у localStorage чи sessionStorage,
 особливо якщо вони вже не потрібні.

3.
Коли завершується сеанс браузера, дані, збережені в sessionStorage, будуть автоматично видалені. 
sessionStorage призначений для тимчасового збереження даних, які пов'язані з конкретною сесією браузера. Це означає, 
що дані в sessionStorage будуть доступні тільки протягом часу життя поточної вкладки браузера. Коли користувач закриє вкладку або вікно браузера,
 сесія завершиться, і дані в sessionStorage будуть автоматично видалені, що допомагає забезпечити приватність і безпеку користувача.



*/









const themeToggle = document.getElementById('themeToggle');
const body = document.body;

themeToggle.addEventListener('click', function() {
    if (body.classList.contains('light-theme')) {
        body.classList.remove('light-theme');
        body.classList.add('dark-theme');
        localStorage.setItem('theme', 'dark');
    } else {
        body.classList.remove('dark-theme');
        body.classList.add('light-theme');
        localStorage.setItem('theme', 'light');
    }
    console.log(body.classList); 
});

document.addEventListener('DOMContentLoaded', function() {
    const savedTheme = localStorage.getItem('theme');
    if (savedTheme) {
        body.classList.add(savedTheme + '-theme');
        console.log("savedTheme");
    }
});
