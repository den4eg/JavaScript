/*  
Теоретичні питання

1. Як можна створити рядок у JavaScript?

2. Яка різниця між одинарними (''), подвійними ("") та зворотніми (``) лапками в JavaScript?

3. Як перевірити, чи два рядки рівні між собою?

4. Що повертає Date.now()?

5. Чим відрізняється Date.now() від new Date()?

----------------------------------------------------------------------------------------

1. В JavaScript рядки можна створити, заключивши текст у лапки одного з трьох типів: 
подвійні лапки (“”), одинарні лапки (‘’), або зворотні лапки (``). Ось приклади:

const singleQuotedString = 'Це одинарний рядок.'; 
const doubleQuotedString = "Це подвійний рядок."; 
const backtickString = `Це рядок зі зворотніми лапками.`;


2. Рядок, створений за допомогою одинарних лапок, подвійних лапок або обгорток,
 генерується як примітивне значення, подібно до чисел та булевих значень. 
 Примітивні дані є незмінними, що означає, що їх не можна змінювати.
 Також вони не мають жодних методів або властивостей.

 Шаблонні літерали (рядки, створені за допомогою обгорток, ``) мають деякі спеціальні функції.
 Одна з них - можливість відображення тексту на декількох рядках, легко та просто.
Але шаблонні літерали надають можливість, яка називається вставка рядка, яка спрощує зрозумілість,
 та робить код більш гнучким.



 3. У JavaScript для перевірки рівності двох рядків між собою можна використовувати оператор порівняння === або
  метод localeCompare() для порівняння посимвольно.

let str1 = "Привіт";
let str2 = "Привіт";

if (str1 === str2) {
    console.log("Рядки рівні");
} else {
    console.log("Рядки не рівні");
}
2. Используя метод localeCompare():
let str1 = "Привіт";
let str2 = "Привіт";

if (str1.localeCompare(str2) === 0) {
    console.log("Рядки рівні");
} else {
    console.log("Рядки не рівні");
}


4. Date.now() кількість мілісекунд з 01.01.1970.

5.Різниця між ними в тому, що New Date() поверне об'єкт Date , а Date. now() - кількість мілісекунд,
 що минула після першого січня 1970 року. Якщо потрібно порівняти дві дати, збережені в різних форматах,
 то ти завжди можеш перевести об'єкт Date в мілісекунди, використовуючи вбудовану функцію getTime() .


*/


// ------------------------------------------------------------------------------------------------



// 1. Перевірити, чи є рядок паліндромом. Створіть функцію isPalindrome, яка приймає рядок str і повертає true, якщо рядок є паліндромом

// (читається однаково зліва направо і справа наліво), або false в іншому випадку.

function palindrome() {
  do {


    alert("Перевірka, чи є рядок паліндромом");
    let str = prompt("Введіть значення");
    let stringArr = str.split("").reverse().join("");

    if (str == stringArr) {
      console.log("Це значення є паліндромом");
    } else {
      console.log("Це значення не є паліндромом");
    } isRepeat = prompt("Do you want to continue (yes/no)").toLowerCase() === "yes";
  } while (isRepeat);
}
palindrome();




// 2. Створіть функцію, яка перевіряє довжину рядка. Вона приймає рядок,
//  який потрібно перевірити, максимальну довжину і повертає true, якщо рядок менше або дорівнює вказаній довжині,
//  і false, якщо рядок довший. Ця функція стане в нагоді для валідації форми.



function names(userName) {
  do {
    userName = prompt("Введіть свій нік щонайменше 6 символів");
    if (userName.length === 0) {
      alert("Введіть свій нік щонайменше 6 символів")
    }
    if (userName.length < 6) {
      alert(`ваш нік ${userName}, коротше 6 символів`);
    } else {
      alert(`Ім'я користувача ${userName}, прийнято!`);
    }
    isRepeat = prompt("Do you want to continue (yes/no)").toLowerCase() === "yes";
    console.log(userName, (userName.length));
  } while (isRepeat);
}
names()


// 3. Створіть функцію, яка визначає скільки повних років користувачу.
//  Отримайте дату народження користувача через prompt.
//  Функція повина повертати значення повних років на дату виклику функцію.


function calculateAge() {
  do {
    const birthday = prompt("Введіть ваш день народження числами (день. місяць. рік)",);
    const parts = birthday.split('.');
    const birthDay = parseInt(parts[0]);
    const birthMonth = parseInt(parts[1]) - 1;
    const birthYear = parseInt(parts[2]);
    const today = new Date();
    const birthDate = new Date(birthYear, birthMonth, birthDay);
    let age = today.getFullYear() - birthDate.getFullYear();
    const monthDiff = today.getMonth() - birthDate.getMonth();
    if (monthDiff < 0 || (monthDiff === 0 && today.getDate() < birthDate.getDate())) {
      age--;
    }
    alert(`ОгОООО Вам ${age} ${age === 1 ? "рік" : "років"}!`);
    isRepeat = prompt("Do you want to continue (yes/no)").toLowerCase() === "yes";
  } while (isRepeat);
}
calculateAge();









// --------------------------------------------------------------------------------
