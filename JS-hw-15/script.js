/* Теоретичні питання

1. В чому відмінність між setInterval та setTimeout?

2. Чи можна стверджувати, що функції в setInterval та setTimeout будуть виконані рівно через той проміжок часу, який ви вказали?

3. Як припинити виконання функції, яка була запланована для виклику з використанням setTimeout та setInterval?



1.
Головна відмінність між `setInterval` та `setTimeout` полягає в тому, як вони запускають функції затримки.

1. **setTimeout**: Ця функція запускає виконання функції один раз через певний період часу, вказаний у мілісекундах. 
Після того, як вказаний період часу мине, функція буде виконана один раз. Наприклад, 
`setTimeout(myFunction, 1000)` запустить `myFunction` через 1 секунду.

2. **setInterval**: Ця функція також запускає виконання функції, але вона робить це періодично, зазначаючи,
 що функція буде виконуватися через певні інтервали часу.



2.
Ні, не можна стверджувати з абсолютною впевненістю, що функції, викликані з setInterval або setTimeout, 
будуть виконані рівно через той проміжок часу, який вказали.
Існує декілька факторів, які можуть впливати на точність часу виконання:

Внутрішній завдання (Event Loop): Якщо JavaScript виконує важкі операції або має багато інших завдань для обробки, 
це може затримати виконання функцій в setInterval або setTimeout.
Системна завантаженість: Якщо комп'ютер або пристрій перевантажений, це може призвести до затримок у виконанні JavaScript,
 що вплине на точність часу виконання.
Точність таймерів: Точність таймерів JavaScript може відрізнятися залежно від браузера та середовища виконання. 
У деяких випадках таймери можуть бути відхилені через оптимізації браузера або ОС.

3.
Для припинення виконання функції, яка була запланована для виклику з використанням setTimeout або setInterval, 
використовуються методи clearTimeout і clearInterval відповідно.



-----------------------------------------------------------------------------------------
Практичне завдання 1:

-Створіть HTML-файл із кнопкою та елементом div.

-При натисканні кнопки використовуйте setTimeout, щоб змінити текстовий вміст елемента div через затримку 3 секунди.
 Новий текст повинен вказувати, що операція виконана успішно.


Практичне завдання 2:

Реалізуйте таймер зворотного відліку, використовуючи setInterval. 
При завантаженні сторінки виведіть зворотний відлік від 10 до 1 в елементі div.
 Після досягнення 1 змініть текст на "Зворотній відлік завершено".

*/

function changeText() {
  const contentDiv = document.getElementById("content");
  contentDiv.innerText = "Зміна тексту...";

  setTimeout(() => {
    contentDiv.innerText = "Операція виконана успішно!";
  }, 4000);
}




const countdownDiv = document.getElementById("countdown");
let count = 10;

const countdownInterval = setInterval(() => {
  if (count > 0) {
    countdownDiv.innerText = count;
    count--;
  } else {
    clearInterval(countdownInterval);
    countdownDiv.innerText = "Зворотній відлік завершено";
  }
}, 1000);






// ---------------------------------------------------------------------------------------------------------------





let p = document.querySelector('p'),
p_width = p.getBoundingClientRect().width

function addBubbles() {
for(var i=0;i<p_width/3;i++) {
let b = document.createElement('div')
b.className = 'bubble'
b.style.width = Math.random() < .5 ? '30px' : '50px'
b.style.left = Math.random() * (p_width - 50) + 'px'
b.style.bottom = 10 * Math.random() + 'px'
b.style.animationDelay = 4 * Math.random() + 's'
p.appendChild(b)
}	
}
addBubbles()


const themeToggle = document.getElementById('themeToggle');
const body = document.body;

themeToggle.addEventListener('click', function() {
    if (body.classList.contains('light-theme')) {
        body.classList.remove('light-theme');
        body.classList.add('dark-theme');
        localStorage.setItem('theme', 'dark');
    } else {
        body.classList.remove('dark-theme');
        body.classList.add('light-theme');
        localStorage.setItem('theme', 'light');
    }
    console.log(body.classList); 
});








