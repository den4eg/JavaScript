/* 
Теоретичні питання
1. Як можна визначити, яку саме клавішу клавіатури натиснув користувач?
2. Яка різниця між event.code() та event.key()?
3. Які три події клавіатури існує та яка між ними відмінність?




1.

Ви можете визначити, яку саме клавішу клавіатури натиснув користувач за допомогою обробника подій keydown.
 Коли користувач натискає будь-яку клавішу, спрацьовує цей обробник,
  і ви можете отримати інформацію про натискану клавішу.

document.addEventListener('keydown', function(event) {
  // Отримання коду клавіші
  const keyCode = event.keyCode;

  // Отримання символу клавіші
  const key = event.key;

  // Виведення отриманої інформації в консоль
  console.log('Код клавіші:', keyCode);
  console.log('Символ клавіші:', key);
});

2.
`event.code` та `event.key` - це дві різні властивості об'єкта події `keydown`,
 які представляють інформацію про клавішу, яку натиснув користувач.

  `event.code`: Ця властивість повертає фізичний код клавіші, 
який представляє собою фактичну фізичну клавішу на клавіатурі. 
Наприклад, клавіша "A" має код "KeyA", клавіша "Enter" - "Enter", 
клавіша "Shift" - "ShiftLeft" або "ShiftRight".

   `event.key`: Ця властивість повертає символ, який відповідає клавіші, 
який натиснув користувач. Наприклад, якщо користувач натискає клавішу "A", 
значення `event.key` буде "a". Для функціональних клавіш,
 таких як "Enter", "Shift", "Backspace", "ArrowUp" і т. д., `event.key` поверне рядок,
  що відповідає назві цієї клавіші.

Отже, основна різниця полягає в тому, що `event.code` повертає фізичний код клавіші,
 а `event.key` повертає символ, який відповідає цій клавіші.


3.
Існує три основні події клавіатури в JavaScript:

1. `keydown`: Ця подія спрацьовує, коли користувач натискає клавішу клавіатури. 
Вона виникає, коли клавіша затиснута. Ця подія відбувається перед `keypress`.

2. `keypress`: Ця подія спрацьовує, коли користувач натискає клавішу,
 що виводить символ (наприклад, букву, цифру, знак пунктуації і т. д.). 
 Вона виникає після `keydown`, але перед `keyup`.

3. `keyup`: Ця подія спрацьовує, коли користувач відпускає клавішу клавіатури. 
Вона виникає, коли клавіша відпущена.

Відмінність між ними полягає в часі спрацьовування та в тому, які дії вони визначають:

- `keydown`: Спрацьовує, коли клавіша затиснута. Вона викликається першою, навіть перед тим,
 як користувач відпустить клавішу, і може відбутися безпосередньо перед `keypress`.
  
- `keypress`: Спрацьовує, коли користувач натискає клавішу, яка виводить символ. 
Вона відбувається після `keydown`, але перед `keyup`.
  
- `keyup`: Спрацьовує, коли клавіша відпущена. Вона викликається останньою, після `keydown` і `keypress`.

Отже, коли користувач натискає клавішу, спочатку спрацьовує `keydown`, потім `keypress`,
 якщо це клавіша, яка виводить символ, і, нарешті, `keyup`, коли клавішу відпускають.






*/
 


// ------------------------------------------------------------------------------------------







// Практичне завдання.
// Реалізувати функцію підсвічування клавіш.

// Технічні вимоги:

// - У файлі index.html лежить розмітка для кнопок.
// - Кожна кнопка містить назву клавіші на клавіатурі
// - Після натискання вказаних клавіш - та кнопка, на якій написана ця літера, 
// повинна фарбуватися в синій колір.
//  При цьому якщо якась інша літера вже раніше була пофарбована в синій колір - вона стає чорною.
//   Наприклад за натисканням Enter перша кнопка забарвлюється у синій колір.
//  Далі, користувач натискає S, і кнопка S забарвлюється в синій колір, 
//   а кнопка Enter знову стає чорною.
// Але якщо при натискані на кнопку її  не існує в розмітці, 
// то попередня активна кнопка повина стати неактивною.



const buttons = document.querySelectorAll('.btn');


let activeButton = null;


buttons.forEach(button => {
    button.addEventListener('click', () => {
        if (activeButton) {
            activeButton.style.color = 'black';
        }
        button.style.color = ' purple';
        activeButton = button;
    });
});


document.addEventListener('keydown', event => {
    const key = event.key.toUpperCase();


    const targetButton = Array.from(buttons).find(button => button.textContent === key);
    
    if (targetButton) {
        if (activeButton) {
            activeButton.style.color = 'rad';
        }
        targetButton.style.color = 'blue';
        activeButton = targetButton;
    }
});




// ---------------------------------------------------------------------------------------------------


let p = document.querySelector('p'),
		p_width = p.getBoundingClientRect().width

function addBubbles() {
	for(var i=0;i<p_width/3;i++) {
		let b = document.createElement('div')
		b.className = 'bubble'
		b.style.width = Math.random() < .5 ? '30px' : '50px'
		b.style.left = Math.random() * (p_width - 50) + 'px'
		b.style.bottom = 10 * Math.random() + 'px'
		b.style.animationDelay = 4 * Math.random() + 's'
		p.appendChild(b)
	}	
}
addBubbles()











