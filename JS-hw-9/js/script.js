/*  
Теоретичні питання

1. Опишіть своїми словами що таке Document Object Model (DOM)

2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?

3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?

4. Яка різниця між nodeList та HTMLCollection?
--------------------------------------------------------------------------------------------------

1. Document Object Model (Об'єктна модель документа) - це програмний інтерфейс,
 який представляє структуру документа у вигляді дерева об'єктів.
  У контексті веб-браузерів документ зазвичай вказує на HTML- або XML-документ.
  DOM надає можливість скриптам (зазвичай написаним на JavaScript) взаємодіяти та маніпулювати структурою,
   стилем та вмістом веб-документів динамічно.


2. innerHTML та innerText - це дві різні властивості в HTML та JavaScript,
 які використовуються для роботи з вмістом елементів на сторінці.
 innerHTML:
Ця властивість дає змогу отримати або встановити HTML вміст внутрішнього вмісту елемента.
innerText:
Ця властивість дозволяє отримати або встановити текстовий вміст внутрішнього вмісту елемента.

3. document.getElementById(): 
Цей метод використовується для отримання посилання на елемент за його унікальним ідентифікатором (id).
document.getElementsByClassName(): 
Цей метод повертає колекцію елементів, що мають заданий клас.
document.getElementsByTagName(): 
Цей метод повертає колекцію елементів з вказаною назвою тегу.
document.querySelector(): 
Цей метод повертає перший елемент, який збігається з CSS-селектором.
document.querySelectorAll(): 
Цей метод повертає всі елементи, які збігаються з CSS-селектором.
Кращий спосіб залежить від конкретного використання. getElementById є найшвидшим і оптимальним.

4.HTMLCollection та NodeList – це дуже схожі на масив колекції. 
Вони зберігають елементи веб-сторінки (вузли DOM).
NodeList може зберігати будь-які типи вузлів, а HTMLCollection – лише вузли HTML елементів. 
До елементів колекцій можна звертатися за індексом, але вони не мають звичних методів масиву.


*/





// ------------------------------------------------------------------------------------------------

// 1. Знайдіть всі елементи з класом "feature", запишіть в змінну, вивести в консоль.
// Використайте 2 способи для пошуку елементів.
// Задайте кожному елементу з класом "feature" вирівнювання тексту по - центру(text-align: center).


// Метод 1: Використання методу getElementsByClassName()
const featureElements1 = document.getElementsByClassName("feature");
for (let i = 0; i < featureElements1.length; i++) {
    featureElements1[i].style.textAlign = "center";
}
console.log("Метод 1 (getElementsByClassName):", featureElements1);

// Метод 2: Використання методу querySelectorAll()
let featureElements2 = document.querySelectorAll(".feature");
featureElements2.forEach(function(element) {
    element.style.textAlign = "center";
});
console.log("Метод 2 (querySelectorAll):", featureElements2);



// --------------------------------------------------------------------------------
// 2.Змініть текст усіх елементів h2 на "Awesome feature".

const h2Elements1 = document.getElementsByClassName("feature");
for (let i = 0; i < h2Elements1.length; i++) {
    h2Elements1[i].getElementsByTagName("h2")[0].textContent = "Awesome feature";
}



//-------------------------------------------------------------------------------------

// Знайдіть всі елементи з класом "feature-title" та додайте в кінець тексту елементу знак оклику "!".


const featureTitleElements1 = document.getElementsByClassName("feature-title");
for (let i = 0; i < featureTitleElements1.length; i++) {
    featureTitleElements1[i].textContent += "!";
}




