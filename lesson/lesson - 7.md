# JavaScript


let book = {
  title: "Шлях самурая",
  author: "Іназо Нітобе",
  year: 1989,
  available: true,
};
book.year = 2002;
book.available = false;
book.publisher = "Kyoto University Press";
book.genres = ["філософія", "історія"];
delete book.available;
console.log(book);



let book = {
    title: "Шлях самурая",
    author: "Іназо Нітобе",
    year: 1989,
    available: true,
}

book.year = 2002;
book.available = false;

book.publisher = "Kyoto University Press";
book.genres = ["філософія", "історія"];

delete book.available

console.log(book);

У вас є об'єкт book, який містить інформацію про книгу: назву, автора, рік видання та жанр. 
Ваше завдання — використати цикл for...in для ітерації по всіх властивостях цього об'єкта та виведення 
інформації про книгу у консоль у форматі "ключ: значення".

Початковий код:

const book = {
  title: "Гаррі Поттер і Філософський камінь",
  author: "Дж. К. Роулінг",
  year: 1997,
  genre: "Фентезі"
};

Вимоги до реалізації:
Використайте цикл for...in для перебору властивостей об'єкта book.
Для кожної властивості виведіть у консоль повідомлення у форматі "ключ: значення".

const book = {
  title: "Гаррі Поттер і Філософський камінь",
  author: "Дж. К. Роулінг",
  year: 1997,
  genre: "Фентезі",
};
for (let key in book) {
  console.log(`${key}: ${book[key]}`);
}

-------------------------------------------------------------------------
// let emptyObj = {};
// let person = {
//   name: "Anna",
//   age: 24,
//   "likes programming": true,
// };

//dot notation
// syntax object.propertyName

// let book = {
//   title: "Big Data",
//   year: 2020,
// };

// console.log(book.title);
// console.log(book.year);

//object.propName = newValue

// let user = {
//   name: "Anna",
//   age: 20,
// };

// user.age = 24;

// console.log(user.name);
// console.log(user.age);

//object.newProperty = value
// let user = {
//   name: "Alex",
// };

// user.age = 30;
// user.proffesion = "BE";

// console.log(user);

//delete object.property

//bracket notation
//syntax object["propertyName"]

// let book = {
//   title: "Big Data",
//   year: 2020,
//   "is about programming": true,
// };

// let propName = "testTitle";

// console.log(book["title"]);
// console.log(book["year"]);
// console.log(book["is about programming"]);
// console.log(book[propName]);

//syntax object["propertyName"] = newValue

// let user = {
//   name: "Anna",
//   age: 20,
//   "likes reading": true,
// };

// user["likes reading"] = false;
// console.log(user["likes reading"]);

//object['newProperty'] = value

// let user = {
//   name: "Alex",
// };

// user["age"] = 30;
// user["proffesion"] = "BE";
// user["is proffesional"] = true;

// console.log(user);

// delete user["is proffesional33838"];

// console.log(user);

//delete object['property']

//check property
//in
//syntax "pripertyName" in object
// const car = {
//   make: "Toyota",
//   model: "Camry",
// };

// console.log("model" in car); //true
// console.log("isModelS" in car); //false

//hasOwnProperty()
//syntax object.hasOwnProperty()

// const car = {
//   make: "Toyota",
//   model: "Camry",
// };

// console.log(car.hasOwnProperty("model")); //true
// console.log(car.hasOwnProperty("year")); //false

//is undefined
//syntax object.propertyName !== undefined

// const car = {
//   make: "Toyota",
//   model: "Camry",
//   // text: undefined,
// };

// console.log(car.make !== undefined); //true
// console.log(car.text !== undefined); //false

// constructor function
// function Person(name, age) {
//   this.name = name;
//   this.age = age;

//   this.greet = function () {
//     console.log(`Hello, ${this.name}`);
//   };
// }

// const person1 = new Person("Anna", 24);
// const person2 = new Person("Alex", 30);

// console.log(person1.greet());
// console.log(person2.greet());

// Object.create()
// Object.create(proto, propObject);

// const animal = {
//   isAlive: true,
//   describe: function () {
//     console.log(`This animal is alive: ${this.isAlive}`);
//   },
//   test: {
//     name: "Any",
//   },
// };

// const dog = Object.create(animal);
// dog.describe();

// const cat = Object.create(animal, {
//   name: {
//     value: "Murka",
//     writable: true, //false,
//     enumerable: true,
//     configurable: true,
//   },
// });

// console.log(cat);

// ES6 classes

// class ClassName {
//   constructor(prop1, prop2) {}
// }

// const newObj = new ClassName(arg1, arg2);

// class Person {
//   constructor(name, age) {
//     this.name = name;
//     this.age = age;
//   }

//   greet() {
//     console.log(`Hello, ${this.name}`);
//   }
// }

// const person1 = new Person("Anny", 25);
// const person2 = new Person("Oleg", 27);

// person1.greet();
// person2.greet();

// const person = {
//   name: "Alex",
//   greet: function () {
//     console.log(`Hello, ${this.name}`);
//   },
// };

// for in
// for (let property in object) {
// }

const person = {
  name: "Oleg",
  age: 35,
  profession: "developer",
};

for (const key in person) {
  if (person.hasOwnProperty(key)) {
    console.log(`${key}: ${person[key]}`);
  }
}

---------------------------------------------------------------------------------------------------------------


// function createSettingsManager() {
//   let settings = {};

//   function setSetting() {
//     let key = prompt("Введіть назву налаштування профілю:");
//     let value = prompt("Введіть значення налаштування:");
//     settings[key] = value;
//     alert(`Налаштування ${key} зі значення ${value} було додано`);
//   }

//   function getSetting() {
//     let key = prompt("Введіть назву налаштування, яке ви хочете отримати:");
//     let value = settings[key];
//     if (value) {
//       alert(`Значення налаштування:  ${value}`);
//     } else {
//       alert(`Налаштування з назвою ${key} не знайдено`);
//     }
//   }

//   function updateSetting() {
//     let key = prompt("Введіть назву налаштування, яке ви хочете оновити:");
//     let value = settings[key];
//     let isReadyToUpdate = confirm(
//       `Ви впевнені, що хочете оновити значення налаштвання ${key}?`
//     );

//     if (isReadyToUpdate) {
//       if (value) {
//         let newValue = prompt(`Введіть нове значення налаштування ${key}`);
//         settings[key] = newValue;
//         alert(`Налаштування ${key} оновлено до значення ${newValue}`);
//       } else {
//         alert(`Налаштування з назвою ${key} не знайдено`);
//       }
//     } else {
//       return;
//     }
//   }

//   function deleteSetting() {
//     let key = prompt("Введть назву налаштування, яке ви хочете видалити");
//     let value = settings[key];
//     let isReadyToDelete = confirm(
//       `Ви впевненні, що хочете видалити значення налаштування ${key}`
//     );
//     if (isReadyToDelete) {
//       if (value) {
//         delete settings[key];
//         alert(`Значення налаштування ${key} видалено`);
//       } else {
//         alert(`Налаштування з назвою ${key} не знайдено`);
//       }
//     } else {
//       return;
//     }
//   }

//   return {
//     setSetting,
//     getSetting,
//     updateSetting,
//     deleteSetting,
//   };
// }

// const settingsManager = createSettingsManager();
// const settingsManager1 = createSettingsManager();

// settingsManager.setSetting();
// settingsManager.getSetting();
// settingsManager.updateSetting();
// settingsManager.getSetting();
// settingsManager.deleteSetting();
// settingsManager.getSetting();

function createOrderSystem() {
  let order = {
    totalPrice: 0,
    isDiscountApplied: false,
  };

  function addItem(price) {
    // order.totalPrice = order.totalPrice + price;
    order.totalPrice += price;
  }

  function applyDiscount(discountFunction) {
    if (!order.isDiscountApplied) {
      order.totalPrice = discountFunction(order.totalPrice);
      order.isDiscountApplied = true;
    }
  }

  function discribeOrderPrice() {
    console.log(
      `Загальна вартість замовлення: ${
        order.totalPrice
      }. Чи знижка застосована: ${order.isDiscountApplied ? "так" : "ні"}`
    );
  }

  return { addItem, applyDiscount, discribeOrderPrice };
}

const orderSystem = createOrderSystem();

orderSystem.addItem(200);
orderSystem.addItem(150);
orderSystem.discribeOrderPrice();

function tenPercetDiscount(total) {
  return total * 0.9;
}

orderSystem.applyDiscount(tenPercetDiscount);
orderSystem.discribeOrderPrice();

orderSystem.applyDiscount(tenPercetDiscount);
orderSystem.discribeOrderPrice();


