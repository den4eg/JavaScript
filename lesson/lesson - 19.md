# JavaScript

// const taskInput = document.getElementById("taskInput");
// const addTaskBtn = document.getElementById("addTaskBtn");
// const taskList = document.querySelector("#taskList");

// function addTaskToTheTaskList() {
//   const taskName = taskInput.value.trim();
//   if (taskName !== "") {
//     const taskItem = document.createElement("div");
//     taskItem.classList.add("taskItem");

//     const p = document.createElement("p");
//     p.textContent = taskName;

//     const span = document.createElement("span");
//     span.classList.add("cross-btn");
//     span.textContent = "❌";
//     span.addEventListener("click", function () {
//       taskItem.remove();
//     });

//     taskItem.append(p);
//     taskItem.append(span);
//     taskList.append(taskItem);

//     taskInput.value = "";
//   }
// }

// addTaskBtn.addEventListener("click", addTaskToTheTaskList);

const openModalButton = document.getElementById("openModalBtn");
const modalContainer = document.querySelector("#modalContainer");
const closeModalBtn = document.getElementById("closeModalBtn");

openModalButton.addEventListener("click", () => {
  modalContainer.style.display = "block";
});

closeModalBtn.addEventListener("click", () => {
  modalContainer.style.display = "none";
});



const userData = [
  { name: "John", age: 30 },
  { name: "Alice", age: 25 },
  { name: "Bob", age: 35 }
];

function addUserCard() {
  const userList = document.getElementById("user-List");
  const template = document.getElementById("user-template");

  userData.forEach((user) => {
    const userClone = template.content.cloneNode(true);

    const userNameSpan = userClone.querySelector(".user-name");
    const userAgeSpan = userClone.querySelector(".user-age");

    userNameSpan.textContent = user.name;
    userAgeSpan.textContent = user.age;

    userList.append(userClone);
  });
}

addUserCard();



Elena 19:17
+

Дмитро 19:38
Так же

Андрій Дурицький 19:38
+

Олег Брошко 19:43
const userData = [
  { name: "John", age: 30 },
  { name: "Alice", age: 25 },
  { name: "Bob", age: 35 }
];

function addUserCard() {
  const userList = document.getElementById("user-List");
  const template = document.getElementById("user-template");

  userData.forEach((user) => {
    const userClone = template.content.cloneNode(true);

    const userNameSpan = userClone.querySelector(".user-name");
    const userAgeSpan = userClone.querySelector(".user-age");

    userNameSpan.textContent = user.name;
    userAgeSpan.textContent = user.age;

    userList.append(userClone);
  });
}

addUserCard();
Сообщения, адресованные в "Групповой чат конференции", также будут отображаться в групповом чате конференции в рамках коллективного чата

Olya Pavlova 19:47
+

Андрій Дурицький 19:47
+

Олег Брошко 19:47
+

Дмитро 19:47
+

Колісник Ярослав 19:47
+

Anna Kuhir 19:49
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
  </head>
  <body>
    <h1>User List</h1>
    <div id="user-list"></div>
    <template id="user-template">
      <div class="user-card">
        <p>Name: <span class="user-name"></span></p>
        <p>Age: <span class="user-age"></span></p>
      </div>
    </template>
    <script src="script.js"></script>
  </body>
</html>
const userData = [
  {
    name: "John Doe",
    age: 28,
  },
  {
    name: "Jane Doe",
    age: 28,
  },
  {
    name: "Alice Jonson",
    age: 28,
  },
];

function addUserCard() {
  const userList = document.getElementById("user-list");
  const template = document.getElementById("user-template");

  userData.forEach((user) => {
    const userClone = template.content.cloneNode(true);

    const userNameSpan = userClone.querySelector(".user-name");
    const userAgeSpan = userClone.querySelector(".user-age");

    userNameSpan.textContent = user.name;
    userAgeSpan.textContent = user.age;

    userList.append(userClone);
  });
}

addUserCard();

Андрій Дурицький 20:00
+

Olya Pavlova 20:14
Access to script at 'file:///Users/olhapavlova/Olya%20Developer/Hometasks/Working%20with%20DOM%20el%20ES6/src/script.js' from origin 'null' has been blocked by CORS policy: Cross origin requests are only supported for protocol schemes: http, data, isolated-app, chrome-extension, chrome, https, chrome-untrusted.
index.html:17 
        
        
       GET file:///Users/olhapavlova/Olya%20Developer/Hometasks/Working%20with%20DOM%20el%20ES6/src/script.js net::ERR_FAILED

Олег Брошко 20:14
GET http://127.0.0.1:5500/practice-DOM-4/exportJsFile.js net::ERR_ABORTED 404 (Not Found)

Elena 20:20
Теж помилку маю
Мені показує що тут помилка<script type="module" src="main.js"></script>
коли в source відкрила
Пользователь Антон вышел
Пользователь Ілля Меркулов вышел
Пользователь Колісник Ярослав вышел

Olya Pavlova 20:57
function createFruitCard(fruit) {
    const cardElement = document.createElement('div');
    cardElement.innerHTML = `<span>Name: <strong>${fruit.name}</strong> </br> Price: <strong>${fruit.price}</strong> </span>`;
    return cardElement;
}

export default function displayUserCard(fruit, container) {
    const card = createFruitCard(fruit);
    container.append(card);
}





import createFruitCard from "./FruitCard.js"

const container = document.getElementById("Fruits-container");
    const fruitsData = [
            {
                name: 'Apple',
                price: 20,
            },
            {
                name: 'Orange',
                price: 25,
            },
            {
                name: 'Banana',
                price: 28,
            },
        ];

        fruitsData.forEach((fruit) => {
            createFruitCard(fruit, container);
        });
Пользователь Maria вышел