# JavaScript

// String();
// let numAsStr = String(123); // "123"
// let boolAsStr = String(true); // "true"

// let userName = "user123 ";
// if (userName.length === 0) {
// }
// if (userName.length < 6) {
//   console.log("Ви ввели недостатню кількість символів");
// } else {
//   console.log("Ім'я користувача прийнято");
// }
// console.log(userName.length);

// let str = "Hello, World!";
// str[0] = "J";

// !!!!!!!!!
// str = "J" + str.substring(1);
// console.log(str);

// console.log(str[5]);
// console.log(str[6]);
// console.log(str.charAt(5));
// console.log(str.charAt(6));

// let str1 = "Hello, ";
// let str2 = "World!";
// let res = str1 + str2; //"Hello, World!"
// console.log(res);

//concat()
// str1.concat(str2, str3, ...., strN)
// let hello = "Hello, ";
// let world = "World!";
// let res = hello.concat(world);
// console.log(res); //"Hello, World!"

// let hello = "Hello,";
// let world = "World!";
// let res = `${hello} ${world}`;
// console.log(res); //"Hello, World!"

// ===, ==, >, <, <=, >=
// console.log("apple" === "Apple");

// toLowerCase(), toUpperCase()

// let greeting = "Hello, WORLD!";
// let lowGreeting = greeting.toLowerCase();
// let upperGreeting = greeting.toUpperCase();

// console.log(lowGreeting);
// console.log(upperGreeting);

// includes()
// let greeting = "Hello, World!";
// console.log(greeting.toLowerCase().includes("World")); //false
// console.log(greeting.includes("H", 1)); //false
// console.log(greeting.includes("h")); //false
// console.log(greeting.includes("goodbye")); //false

// indexOf()
// let greeting = "Hello, World! World is beautiful";
// console.log(greeting.indexOf("World", 13)); //14
// console.log(greeting.indexOf("world")); //-1
// console.log(greeting.indexOf("i")); //20

// startWith(), endWith()
// let greeting = "Hello, World! World is beautiful";
// console.log(greeting.startsWith("Hello")); //true
// console.log(greeting.startsWith("H")); //true
// console.log(greeting.startsWith("World")); //false
// console.log(greeting.startsWith("W")); //false
// console.log(greeting.startsWith(" World", 6)); //true

// console.log(greeting.endsWith("beautiful")); //true
// console.log(greeting.endsWith("b")); //false
// console.log(greeting.endsWith("u", greeting.length - 1)); //true

//slice
// let greeting = "Hello, World!";
// console.log(greeting.slice(7, 12)); //World
// console.log(greeting.slice(-5, -1)); //orld

// console.log(greeting.substring(12, 7)); //World
// console.log(greeting.substr(7, 5)); //World

//split and join
let fruits = "aplle, banana, orange";
let fruitsArr = fruits.split(", ");
console.log(fruitsArr.join(", "));





-----------------------------------------------------------------------------------------------

// let userName = "user123 ";
// if (userName.length === 0) {
// }
// if (userName.length < 6) {
//   console.log("Ви ввели недостатню кількість символів");
// } else {
//   console.log("Ім'я користувача прийнято");
// }
// console.log(userName.length);



// function longestPalindrome(s)
// {
  
//   if (s === '') {
//     return s;
//   }
//   let maxLength = 0;
//   let start = 0;
  
//   for (let i = 0; i < s.length; i++) {
//     expandFromCenter(s, i, i);
//     expandFromCenter(s, i, i + 1);
//   }
//   return s.substring(start, start + maxLength);
  
//   function expandFromCenter(s, low, high) {
//     while (low > -1 && high < s.length && s[low] === s[high]) {
//       low -= 1;
//       high += 1;
//     }
    
//     const len = high - low - 1;
//     if (maxLength < len) {
//       maxLength = len;
//       start = low + 1;
//     }
//   }
// }

// // const s = 'tracecars';
// const s = prompt("Введите значение");
// console.info(longestPalindrome(s));


// function palindrome(str) {
//     return str.split('').reverse().join('') == str;
//   }
  
// function palindrome(str) {
//     let check = '';
//     for (let i = str.length; i > 0; --i) {
//         check += str[i];
//     }
//     if (str == check) {
//         return true;
//     } else {
//         return false;
//     }
// }
// console.log(check);

// console.log(palindrome('34543'));// === true
// console.log(palindrome('345543'));// === true
// console.log(palindrome('3'));// === true
// console.log(palindrome(''));// === true
// console.log(palindrome('723210'));// === false