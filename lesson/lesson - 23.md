# JavaScript

// document.addEventListener("keydown", function (event) {
//   console.log(
//     `User select btn with code: ${event.code}, with key: ${event.key}`
//   );
// });

// document.addEventListener("keyup", function (event) {
//   console.log(`Key code: ${event.code}, key: ${event.key}`);
// });

// document
//   .getElementById("first-input")
//   .addEventListener("keydown", function (event) {
//     if (event.key === "Enter") {
//       event.preventDefault();
//       console.log(event.key);
//       console.log("Ви натиснули на Enter без відправлення форми");
//     }
//   });

// document.addEventListener("scroll", function () {});

// const navbar = document.getElementById("navbar");

// window.addEventListener("scroll", function () {
//   let scrollTop = document.documentElement.scrollTop; //in px
//   let scrollLeft = document.documentElement.scrollLeft; //in px

//   console.log(scrollLeft);

//   if (scrollTop > 50) {
//     navbar.style.backgroundColor = "rgba(0, 0, 0, 0.8)";
//   } else {
//     navbar.style.backgroundColor = "rgba(0, 0, 0, 0.5)";
//   }
// });

// const form = document.forms.first;
// const inputEl = form.elements.username;
// inputEl.value = "Ihor";
// console.log(inputEl.value);

// const lastNameInput = document.querySelector('input[name="lastName"]');

// const form = lastNameInput.form;

// form.addEventListener("submit", function (event) {
//   console.log(`Форма відправлена, ім'я: ${form.firstName.value}`);
//   event.preventDefault();
// });

// const select = document.getElementById("select");

// select.options[2].selected = true;
// select.value = "pear";
// select.selectedIndex = 2;

// const select = document.getElementById("select");

// const mango = new Option("Mango", "mango", false, true);

// select.add(mango);

const form = document.getElementById("registration");

// form.elements.firstName.addEventListener("focus", function () {
//   console.log("Елемент у фокусі");
// });
// form.elements.firstName.addEventListener("blur", function () {
//   console.log("Фокус з елемента прибраний");
// });

form.elements.firstName.addEventListener("change", function (event) {
  console.log(event.target.value);
});

form.elements.firstName.addEventListener("input", function (event) {
  console.log(event.target.value);
});



<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
    <style>
      #navbar {
        position: fixed;
        top: 0;
        background-color: rgba(0, 0, 0, 0.5);
        color: white;
        padding: 10px;
      }
      body {
        height: 2000px;
      }
    </style>
  </head>
  <body>
    <!-- <form action="">
      <input id="first-input" />
    </form> -->

    <!-- <div id="navbar">Navbar menu</div> -->

    <!-- <form id="first" name="first" action="">
      <input type="text" name="username" value="Anna" />
    </form>
    <form id="second" name="second" action="">
      <input type="text" name="second_name" />
    </form>
    <form id="third" name="third" action="">
      <input type="number" name="age" />
    </form> -->

    <form id="registration" action="">
      <input type="text" name="firstName" value="Anna" tabindex="1" />
      <input type="text" name="lastName" tabindex="-1" />
      <button type="submit">Sign in</button>
    </form>

    <!-- <select id="select">
      <option value="apple">Apple</option>
      <option value="pear">Pear</option>
      <option value="banana">Banana</option>
    </select> -->
    <script src="script.js"></script>
  </body>
</html>
