# JavaScript

// Функція для створення картки тренера (з вашого коду)
function createTrainerCard(trainer) {
    // Код для створення карти тренера
}

// Функція для відображення тренерів з врахуванням вибраних фільтрів
function displayFilteredTrainers() {
    // Отримання вибраних значень напряму та категорії
    const selectedDirection = document.querySelector('input[name="direction"]:checked').value;
    const selectedCategory = document.querySelector('input[name="category"]:checked').value;
    
    // Отримання контейнера, в який будуть додаватися відфільтровані тренери
    const trainersContainer = document.getElementById('trainers-container');
    
    // Очищення контейнера перед додаванням нових тренерів
    trainersContainer.innerHTML = '';

    // Фільтрація тренерів на основі вибраних параметрів та додавання відповідних карток
    DATA.filter(trainer => {
        // Перевірка на відповідність вибраним параметрам
        return (selectedDirection === 'all' || trainer.specialization.toLowerCase().includes(selectedDirection)) &&
               (selectedCategory === 'all' || trainer.category.toLowerCase() === selectedCategory);
    }).forEach(trainer => {
        // Створення та додавання карти тренера до контейнера
        trainersContainer.appendChild(createTrainerCard(trainer));
    });
}

// Отримання кнопки "ПОКАЗАТИ" та додавання обробника подій
const showButton = document.querySelector('.filters__submit');
showButton.addEventListener('click', function(event) {
    event.preventDefault(); // Заборона дії за замовчуванням (перезавантаження сторінки)
    displayFilteredTrainers(); // Виклик функції для відображення відфільтрованих тренерів
});

// Початкове відображення всіх тренерів при завантаженні сторінки
displayFilteredTrainers();
