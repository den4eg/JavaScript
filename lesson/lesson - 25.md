# JavaScript

// const notesList = document.getElementById("notesList");
// const noteInput = document.getElementById("noteInput");
// const addNoteBtn = document.getElementById("addNoteBtn");

// function updateNotes() {
//   const notes = JSON.parse(localStorage.getItem("notes")) || [];
//   notesList.innerHTML = "";
//   notes.forEach((note, index) => {
//     const li = document.createElement("li");
//     li.textContent = note;

//     const editBtn = document.createElement("button");
//     editBtn.textContent = "Edit";
//     editBtn.addEventListener("click", function () {
//       editNote(index);
//     });

//     const deleteBtn = document.createElement("button");
//     deleteBtn.textContent = "Delete";
//     deleteBtn.addEventListener("click", function () {
//       deleteNote(index);
//     });

//     li.append(editBtn, deleteBtn);
//     notesList.append(li);
//   });
// }

// function addNote() {
//   const newNote = noteInput.value.trim();
//   if (newNote !== "") {
//     const notes = JSON.parse(localStorage.getItem("notes")) || [];
//     notes.push(newNote);

//     localStorage.setItem("notes", JSON.stringify(notes));
//     updateNotes();
//     noteInput.value = "";
//   }
// }

// function deleteNote(index) {
//   const notes = JSON.parse(localStorage.getItem("notes")) || [];
//   if (confirm("Are you sure you want to delete this note?")) {
//     notes.splice(index, 1);
//     localStorage.setItem("notes", JSON.stringify(notes));
//     updateNotes();
//   }
// }

// function editNote(index) {
//   const notes = JSON.parse(localStorage.getItem("notes")) || [];
//   const editedNote = prompt("Edit note:", notes[index]);
//   if (editedNote !== "") {
//     notes[index] = editedNote;
//     localStorage.setItem("notes", JSON.stringify(notes));
//     updateNotes();
//   }
// }

// updateNotes();
// addNoteBtn.addEventListener("click", addNote);

// const themeSelect = document.getElementById("themeSelect");

// function changeTheme() {
//   const selectedTheme = themeSelect.value;
//   const link = document.getElementById("themeStylesheet");
//   link.href = `styles/${selectedTheme}-theme.css`;
//   localStorage.setItem("theme", selectedTheme);
// }

// import { themeSelect, changeTheme } from "./changeTheme.js";

// const savedTheme = localStorage.getItem("theme");
// if (savedTheme) {
//   themeSelect.value = savedTheme;
//   changeTheme();
// }

// themeSelect.addEventListener("change", changeTheme);

// const users = [
//   {
//     name: "Aнна",
//     surname: "Петренко",
//     job: "Кассир",
//     age: 25,
//   },
//   {
//     name: "Василь",
//     surname: "Петренко",
//     job: "Охоронець",
//     age: 30,
//   },
//   {
//     name: "Влад",
//     surname: "Павленко",
//     job: "Продавець",
//     age: 25,
//   },
//   {
//     name: "Олег",
//     surname: "Павлов",
//     job: "Кухар",
//     age: 38,
//   },
//   {
//     name: "Вікторія",
//     surname: "Любченко",
//     job: "Бугалтер",
//     age: 30,
//   },
// ];

// const sortedUsersByName = users.sort((a, b) => {
//   return a.name.localeCompare(b.name, "ua");
// });

// console.log(sortedUsersByName);


// function startCountdown() {
//   let startTime = 10;

//   const timerElement = document.getElementById("timer");
//   timerElement.textContent = startTime;

//   const intervalId = setInterval(() => {
//     startTime--;
//     timerElement.textContent = startTime;

//     if (startTime <= 0) {
//       clearInterval(intervalId);
//       setTimeout(() => {
//         alert("Час вийшов");
//       }, 0);
//     }
//   }, 500);
// }

// const startBtn = document.getElementById("start-btn");
// startBtn.addEventListener("click", startCountdown);

// const user = [
//   { name: "Anna", age: 15 },
//   { name: "Anton", age: 20 },
//   { name: "Dmytro", age: 35 },
// ];

// function setUserCard() {
//   const cardWrapper = document.getElementById("data-wrapper");

//   user.map((user) => {
//     const userCard = document.createElement("div");
//     const header = document.createElement("h2");
//     const info = document.createElement("p");

//     header.textContent = `User Name: ${user.name}`;
//     info.textContent = `User Age: ${user.age}`;

//     userCard.append(header, info);
//     cardWrapper.append(userCard);
//     console.log("Завантаження карточок завершено");
//   });
// }

// const startBtn = document.getElementById("start-btn");
// const resetBtn = document.getElementById("reset-btn");
// let timerId;

// startBtn.addEventListener("click", () => {
//   // console.log("Before timeout");
//   timerId = setTimeout(setUserCard, 3000);
//   // console.log("After timeout");
// });

// resetBtn.addEventListener("click", () => {
//   if (timerId) {
//     clearTimeout(timerId);
//     console.log("Завантаження карточок було скасовано");
//   }
// });

// const user = { userName: "Anna", age: 23 };

// // localStorage.setItem("username", "John");
// // localStorage.setItem("age", "23");
// localStorage.setItem("user", JSON.stringify(user));

// const storUser = localStorage.getItem("user");
// console.log(JSON.parse(storUser));

// localStorage.removeItem("username");
// localStorage.removeItem("age");

// sessionStorage.setItem();
// sessionStorage.getItem();
// sessionStorage.removeItem();

window.addEventListener("storage", function () {
  console.log("Відбулася подія storage");
});

function changeStorage() {
  localStorage.setItem("message", "Hello!");
}

const changeStorageBtn = document.getElementById("start-btn");
changeStorageBtn.addEventListener("click", () =>
  setTimeout(changeStorage, 3000)
);

