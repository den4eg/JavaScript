

Anna Kuhir 19:04
let recipes = [
  {
    name: "Томатний суп",
    ingredients: ["томати", "вода", "сіль"],
    instructions: "Змішайте всі інгредієнти та варіть 20 хвилин.",
  },
];

Anna Kuhir 19:10
addRecipe(
  "Омлет",
  ["яйця", "молоко", "сіль"],
  "Змішайте всі інгредієнти, смажте на сковороді  5-10 хвилин."
);

console.log(recipes); //[{}, {}]

Elena 19:19
Так
Є
Готова вже)

Dmytro Riabovol 19:21
+

Elena 19:22
function addRecipe(initialArray, name, ingredients, instructions) {
  const newId =
    initialArray.reduce((acc, curr) => {
      return curr.id > acc ? curr.id : acc;
    }, 0) + 1;

  const recipe = {
    name: name,
    ingredients: ingredients,
    instructions: instructions,
  };
  const newRecipe = { id: newId, ...recipe };
  initialArray.push(newRecipe);
  console.log("New recipe was added");
}
addRecipe(
  recipes,
  "Омлет",
  ["яйця", "молоко", "сіль"],
  "Змішайте всі інгредієнти, смажте на сковороді  5-10 хвилин."
);

addRecipe(
  recipes,
  "Млинці",
  ["яйця", "молоко", "борошно"],
  "Змішайте всі інгредієнти, смажте на сковороді з обох боків по 2-3хв."
);
console.log(recipes);

Олег Брошко 19:22
let recipes = [
  {
    id: 0,
    name: "Томатний суп",
    ingredients: ["томати", "вода", "сіль"],
    instructions: "Змішайте всі інгредієнти та варіть 20 хвилин.",
  },
];

function addRecipe(name, ingredients, instructions) {
  return recipes.push(name, ingredients, instructions);
}

addRecipe(
  "Омлет",
  ["яйця", "молоко", "сіль"],
  "Змішайте всі інгредієнти, смажте на сковороді  5-10 хвилин."
);

console.log(recipes);

Dmytro Riabovol 19:22
let recipes = [
    {
      id: 0,
      name: "Томатний суп",
      ingredients: ["томати", "вода", "сіль"],
      instructions: "Змішайте всі інгредієнти та варіть 20 хвилин.",
    },
  ];
  
  function addRecipe(name, ingredients, instructions) {
    let newRecipe = {
      id: 1,
      name: "Омлет",
      ingredients: ["яйця", "молоко", "сіль"],
      instructions:  "Змішайте всі інгредієнти, смажте на сковороді 5-10 хвилин.",
    };
    recipes.push(newRecipe);
  }
  
  addRecipe(
    "Омлет",
    ["яйця", "молоко", "сіль"],
    "Змішайте всі інгредієнти, смажте на сковороді 5-10 хвилин."
  );
  
  console.log(recipes);

Elena 19:25
Я з id створила
Да)

Dmytro Riabovol 19:28
я понял что не так сделал и поменял как у вас
+

Anna Kuhir 19:29
let recipes = [
  {
    name: "Томатний суп",
    ingredients: ["томати", "вода", "сіль"],
    instructions: "Змішайте всі інгредієнти та варіть 20 хвилин.",
  },
  {
    name: "Омлет",
    ingredients: ["яйця", "молоко", "сіль"],
    instructions: "Змішайте всі інгредієнти, смажте на сковороді  5-10 хвилин.",
  },
  {
    name: "Салат",
    іngredients: ["томати", "огірки", "олія"],
    instructions: "Змішайте всі інгредієнти. Додайте спеції за смаком.",
  },
];
Сообщения, адресованные в "Групповой чат конференции", также будут отображаться в групповом чате конференции в рамках коллективного чата

Olya Pavlova 19:47
function removeRecipe(initialArray, recipeName) {
  const index = initialArray.findIndex((name) => name === recipeName);
  if (index !== -1) {
    initialArray.splice(index, 1);
    console.log(`Recipe with ${recipeName} is removed`);
  } else {
    console.log('Product was not found');
  }
}

Не впевнена, що все правильно


І ще одну пробую теж не зовсім вийшло:

  function deleteRecipe(initialArray, name) {
    const filteredProducts = initialArray.filter(
        (product) => product.name.toLowerCase() === name.toLowerCase()
        );
        if (filteredProducts === name) {
          initialArray.splice(filteredProducts, name)
          console.log(`Recipe with ${name} is removed`);
  } else {
    console.log('Product was not found');
  }
  }

Elena 19:50
В мене є питання, можете показати як зробити так, щоб передавати в аргумент виклику функції тільки назву, без передачі назви масиву

Олег Брошко 19:50
function removeRecipe(recipeName) {
  const index = recipes.findIndex((item) => item.name === recipeName);
  if (index !== -1) {
    recipes.splice(index, 1);
    console.log(`product was deleted`);
  } else {
    console.log(`product not found`);
  }
}

removeRecipe("Омлет");
console.log(recipes);

Elena 19:55
Дякую)

Олег Брошко 19:57
function removeRecipe(recipeName) {
  const filterProducts = recipes.filter((item) => {
    item.name.toLowerCase() !== recipeName.toLowerCase();
  });
  return filterProducts;
}
let result = removeRecipe("Омлет");

console.log(result);

Антон 19:59
Надо было по названию удалять рецепт или по индексу?

Андрій Дурицький 20:08
-

Elena 20:08
Так, ще раз)
У мене повернуло

Olya Pavlova 20:12
function findRecipeByIngredients(...ingredients) {
   return recipes2.filter((recipe) => 
    ingredients.every((ingredient) => 
    recipe.ingredients.includes(ingredient))
    );
  }

  console.log('filtered ingredients', findRecipeByIngredients('молоко', 'яйця'));

Elena 20:12
function findRecipesByIngredients(...ingredients) {
  return recipes.filter((recipe) =>
    ingredients.every((ingredient) => recipe.ingredients.includes(ingredient))
  );
}
Пользователь Maria присоединился в качестве гостя

Anna Kuhir 20:26
function findRecipeByIngredients(...ingredients) {
  return recipes2.filter((recipe) =>
    ingredients.every((ingredient) => {
      const ingredientsArr = recipe.ingredients.map((item) =>
        item.toLowerCase()
      );
      ingredientsArr.includes(ingredient.toLowerCase());
    })
  );
}
Пользователь Колісник Ярослав вышел

Maria 20:27
Зрозуміла
Пользователь Yaroslav-kolisnyk Yk присоединился в качестве гостя
Пользователь Антон вышел
Пользователь Ілля Меркулов вышел

Olya Pavlova 20:40
function getListofIngridients(recipeName) {
  let recipeList =recipes2.filter((recipe) => recipeName.toLowerCase() === recipe.ingredients);
  return recipeList;
  }

  console.log('list of ingridients', getListofIngridients('Салат')); 

Щось знову не зовсім так
Пользователь Elena вышел
Пользователь Yaroslav-kolisnyk Yk вышел
Пользователь Yaroslav-kolisnyk Yk присоединился в качестве гостя
Пользователь Yaroslav-kolisnyk Yk вышел
Пользователь Yaroslav-kolisnyk Yk присоединился в качестве гостя

Anna Kuhir 20:49
function getListofIngridients(recipeName) {
  let recipe = recipes2.filter(
    (recipe) => recipe.name.toLowerCase() === recipeName.toLowerCase()
  );
  console.log("recipe", recipe);
  console.log("recipe[0]", recipe[0]);
  return recipe[0].ingredients.join(", ");
}
Пользователь Yaroslav-kolisnyk Yk вышел
Пользователь Yaroslav-kolisnyk Yk присоединился в качестве гостя
Пользователь Yaroslav-kolisnyk Yk вышел

Anna Kuhir 20:57
let recipes2 = [
  {
    name: "Томатний суп",
    ingredients: ["томати", "вода", "сіль"],
    instructions: "Змішайте всі інгредієнти та варіть 20 хвилин.",
  },
  {
    name: "Омлет",
    ingredients: ["яйця", "молоко", "сіль"],
    instructions: "Змішайте всі інгредієнти, смажте на сковороді  5-10 хвилин.",
  },
  {
    name: "Салат",
    ingredients: ["томати", "огірки", "олія"],
    instructions: "Змішайте всі інгредієнти. Додайте спеції за смаком.",
  },
];

function getListofIngridients(recipeName) {
  let recipe = recipes2.filter(
    (recipe) => recipe.name.toLowerCase() === recipeName.toLowerCase()
  );
  console.log("recipe", recipe);
  console.log("recipe[0]", recipe[0]);
  return recipe[0].ingredients.join(", ");
}

console.log("list of ingridients", getListofIngridients("Салат"));
Пользователь Yaroslav-kolisnyk Yk присоединился в качестве гостя