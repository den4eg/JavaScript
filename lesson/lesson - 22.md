# JavaScript

// document.addEventListener("DOMContentLoaded", function () {
//   console.log(
//     "HTML документ повністю завантажений, але зображення ще можуть завантажуватися"
//   );
//   console.log("readyState DOMContentLoaded", document.readyState);
// });

// window.onload = function () {
//   console.log("Усі ресурси сторінки завантажено");
//   console.log("readyState", document.readyState);
//   const image = document.getElementById("image");
//   console.log(
//     "Розміри зорбаженя" + image.offsetWidth + "," + image.offsetHeight
//   );
// };

// window.onunload = function () {
//   // localStorage
//   console.log("Стан збережений");
// };

// document.addEventListener("unload", () => {});

// window.onbeforeunload = function (event) {
//   const textArea = document.getElementById("textArea");
//   if (textArea.value !== "") {
//     let message = "Чи ви впевнені, що хочете покинути сторінку?";
//     event.returnValue = message;
//     return message;
//     // alert("Чи ви впевнені, що хочете покинути сторінку?");
//   }
// };

// document.getElementById("grandparent").addEventListener(
//   "click",
//   function (event) {
//     console.log("Grandparent clicked", event.target);
//   }
//   // true
// );

// document.getElementById("parent").addEventListener(
//   "click",
//   function (event) {
//     console.log("Parent clicked", event.target);
//   }
//   // true
// );

// document.getElementById("child").addEventListener(
//   "click",
//   function (event) {
//     console.log("Button child clicked", event.target);
//     // event.stopPropagation();
//     // event.stopImmediatePropagation();
//   }
//   // true
// );

// document.getElementById("link").addEventListener("click", function (event) {
//   console.log(event.defaultPrevented);
//   event.preventDefault();
//   // console.log(event.defaultPrevented);
//   console.log("Стандартна дія була запобіжена");
// });

// document.getElementById("list").addEventListener("click", function (event) {
//   console.log(event.target.textContent);
// });

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
    <!-- <link 
    <script></script> -->
  </head>
  <body>
    <h1>Browser events</h1>
    <!-- <p>
      Lorem ipsum dolor sit amet consectetur adipisicing elit. Sed laudantium
      porro voluptate, nesciunt natus iusto!
    </p>
    <img id="image" src="large-image.jpeg" alt="Large image" />
    <br />
    <textarea id="textArea"></textarea> -->

    <!-- <div id="grandparent">
      Grandparent
      <div id="parent">
        Parent
        <button id="child">Click on me</button>
      </div>
    </div> -->

    <!-- <a
      id="link"
      href="https://tech.lalilo.com/redux-saga-and-typescript-doing-it-right"
      >Go to React doc</a
    > -->

    <ul id="list">
      <li>List item 1</li>
      <li>List item 2</li>
      <li>List item 3</li>
      <li>List item 4</li>
      <li>List item 5</li>
    </ul>

    <script defer async src="script.js" onload="" onerror=""></script>

    <!-- <script>
      console.log("Це ваш вбудований скрипт");
    </script> -->
  </body>
</html>
