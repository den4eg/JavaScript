# JavaScript

<input type="button" id="hider" value="Click to hide the text" />
<div id="text">Text</div>

Olya Pavlova 19:43
const deleteText = document.getElementById("hider");

deleteText.onclick = function(event) {
    const text = document.getElementById('text');
    text.hidden = true;
}

Дмитро 19:44
const hider = document.getElementById("hider");
const div = document.getElementById("text");

hider.addEventListener("click", function() {
    div.remove();
    // div.style.display = "none";
})

Olya Pavlova 19:45
deleteText.addEventListener('click', function(event)) {
    const text = document.getElementById('text');
    text.hidden = true;
}

Олег Брошко 19:45
const input = document.getElementById("hider");
const div = document.getElementById("text");
input.addEventListener("click", function () {
  div.style.display = "none";
});