# JavaScript

// // console.log(window.document);
// console.log(document.documentElement); //html
// console.log(document.body); //body
// console.log(document.head); //head

// document.body.style.backgroundColor = "red";

// console.log(document.body.childNodes);

// console.log(document.body.firstChild);
// console.log(document.body.lastChild);

// Array.from(document.body.childNodes).filter();

// console.log(document.body.children);
// console.log(document.body.firstElementChild.nextElementSibling);
// console.log(document.body.firstElementChild.previousElementSibling);
// console.log(document.body.lastElementChild);

// console.log(document.body.firstElementChild);
// console.log(document.body.firstElementChild.nextElementSibling);
// // console.log(document.body.firstElementChild.nextElementSibling.lastChild);
// console.log(
//   document.body.firstElementChild.nextElementSibling.firstElementChild
//     .nextElementSibling.textContent
// );

// const adamListItem = document.getElementById("adam-name");
// console.log(adamListItem.textContent);

// const listItems = document.getElementsByClassName("list-item");
// console.log(listItems);

// const fullListItems = document.getElementsByTagName("li");
// console.log(fullListItems);

// const listItem = document.querySelector(".list-item");
// console.log(listItem);

// const listItemsCopy = document.querySelectorAll(".list-item");
// console.log(listItemsCopy);

// document.body.innerHTML = "<h1>New HTML content<h1>";
// console.log(document.body.innerHTML);

// document.body.children[0].textContent = "Hew Title";
// console.log(document.body.children[0].textContent);

// document.body.children[0].hidden = true;
// console.log(document.body.children[0].hidden);

// document.querySelector("#adam-name").innerHTML = "<a href=''>Denis</a>";
--------------------------------------------------------------------------------------
                                   html
<!-- <!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
  </head>
  <body>
    <!-- ]<div>Users:</div>
    <ul class="list">
      <li class="list-item">John</li>
      <li class="list-item">John 2</li>
      <li id="adam-name">Adam</li>
    </ul> -->

    <!-- <ul>
      <li class="list-item">John</li>
      <li class="list-item">John 2</li>
      <li id="adam-name">Adam</li>
    </ul>
    <h1>Title</h1>
    <script src="script.js"></script> -->

    <script>
      let body = document.body;
      body.innerHTML = "<p>My paragraph</p>";
      alert(document.body.lastChild.constructor.name); // Text
    </script>
  </body>
</html> -->



// Функции


// const name = 'John';
// let final = 0;


// if (name === 'Alex') {
// 	const product = createProduct('Арбуз', 100, 1);
// 	const alexCart = [product];

// 	const debt = 150;

// 	final = calculateFinalPrice(alexCart);
// 	final += debt;
// }
// else if (name === 'John') {
// 	const product = createProduct('Хлеб', 'hfeqfq', 2);
// 	const johnCart = [product];

// 	const debt = 50;

// 	final = calculateFinalPrice(johnCart);
// 	final += debt;
// }
// else {
// 	const product = createProduct('Молоко', 50, 1);
// 	const userCart = [product];

// 	final = calculateFinalPrice(userCart);
// }

// console.log(final);

// function createProduct(name, price, count) {
// 	if (name === '') {
// 		name = 'Неизвестный продукт';
// 		console.log('Вы задали неправильное имя продукта');
// 	}

// 	if (typeof price !== 'number') {
// 		price = 0;
// 		console.log('Вы задали неправильную цену');
// 	}

// 	if (typeof count !== 'number') {
// 		count = 0;
// 		console.log('Вы задали неправильное количество');
// 	}

// 	return {
// 		name,
// 		price,
// 		count,
// 	};
// }

// function calculateFinalPrice(cart) {
// 	let finalPrice = 0;

// 	for (let i = 0; i < cart.length; i++) {
// 		const product = cart[i];

// 		finalPrice += (product.price * product.count);
// 	}


// 	return finalPrice;
// }




// ---------------------------------------------------------------------------------------------------
// Стрелочные функции


const name = 'John';
let final = 0;


if (name === 'Alex') {
	const product = createProduct('Арбуз', 100, 1);
	const alexCart = [product];

	const debt = 150;

	final = calculateFinalPrice(alexCart);
	final += debt;
}
else if (name === 'John') {
	const product = createProduct('Хлеб', 30, 2);
	const johnCart = [product];

	const debt = 50;

	calculateFinalPrice(johnCart, (price) => {
		console.log('Финальная цена:', price + debt);
	});
}
else {
	const product = createProduct('Молоко', 50, 1);
	const userCart = [product];

	final = calculateFinalPrice(userCart);
}

function createProduct(name, price, count) {
	if (name === '') {
		name = 'Неизвестный продукт';
		console.log('Вы задали неправильное имя продукта');
	}

	if (typeof price !== 'number') {
		price = 0;
		console.log('Вы задали неправильную цену');
	}

	if (typeof count !== 'number') {
		count = 0;
		console.log('Вы задали неправильное количество');
	}

	return {
		name,
		price,
		count,
	};
}

function calculateFinalPrice(cart, callback) {
	setTimeout(() => {
		let finalPrice = 0;

		for (let i = 0; i < cart.length; i++) {
			const product = cart[i];

			finalPrice += (product.price * product.count);
		}

		callback(finalPrice);
	}, 1000);
}



// function add(a, b) {
// 	return a+b;
// }

// const add = (a, b) => {
// 	return a+b;
// };

// const add = (a, b) => (
// 	a + b + 2 + 3 + 4 + 5 + 6 + 7
// );

// function add(a) {
// 	return a + 5;
// }

// const add = a => a + 5;


// console.log(add(5))


