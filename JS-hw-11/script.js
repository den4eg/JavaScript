/* 
Теоретичні питання
1. Що таке події в JavaScript і для чого вони використовуються?
2. Які події миші доступні в JavaScript? Наведіть кілька прикладів.
3. Що таке подія "contextmenu" і як вона використовується для контекстного меню?


1.
 Події в JavaScript - це механізм, який дозволяє вам реагувати на дії користувачів або на інші події,
які виникають у вашому додатку або на веб-сторінці. Події можуть бути спровоковані різними діями, 
такими як клік мишею, натискання клавіш, завантаження сторінки, зміна розміру вікна браузера тощо.
 Події використовуються для створення інтерактивних та реактивних додатків,
які можуть реагувати на взаємодію користувача та зміни в середовищі виконання.


2. 
 У JavaScript існує низка подій миші, які дозволяють вам реагувати на різні дії користувача з мишею. Ось декілька найпоширеніших подій миші:
click: Виникає при кліку на елемент мишею.
mouseover: Виникає, коли курсор миші наведено на елемент.
mouseout: Виникає, коли курсор миші виведено за межі елементу.
mousemove: Виникає при переміщенні курсора миші над елементом.
contextmenu: Виникає при правому кліку на елементі, показуючи контекстне меню.
mousedown: Виникає при натисканні кнопки миші на елементі.

3.
 Подія "contextmenu" в JavaScript виникає, коли користувач натискає праву кнопку миші на елементі, 
що спричиняє відображення контекстного меню браузера або користувацького контекстного меню, 
якщо воно було встановлено.

Ця подія може бути використана для відображення власних контекстних меню у веб-додатках, 
які відповідають на правий клік користувача на певних елементах сторінки. 
 Ви можете встановити обробник події "contextmenu" на потрібних елементах і
визначити власну логіку обробки цього події, наприклад, відображення власного контекстного меню 
за допомогою HTML і CSS або виконання певних дій за допомогою JavaScript.

*/
















/*

Практичні завдання
 1. Додати новий абзац по кліку на кнопку:
  По кліку на кнопку <button id="btn-click">Click Me</button>, створіть новий елемент <p> з текстом "New Paragraph" і додайте його до розділу <section id="content">
 
 2. Додати новий елемент форми із атрибутами:
 Створіть кнопку з id "btn-input-create", додайте її на сторінку в section перед footer.
  По кліку на створену кнопку, створіть новий елемент <input> і додайте до нього власні атрибути, наприклад, type, placeholder, і name. та додайте його під кнопкою.
 
*/

function newParagraph() {
    const newParagraph = document.createElement('p');
    newParagraph.classList.add("select__p");
    newParagraph.textContent = 'Новесенький пораграф  *(**,)*';

    const deleteButton = document.createElement('button');
    deleteButton.classList.add("deleteButton");
    deleteButton.textContent = 'X';

    deleteButton.addEventListener('click', function () {
        newParagraph.remove();
    });

    newParagraph.append(deleteButton);
    const btnClick = document.getElementById('btn-click');
    btnClick.insertAdjacentElement('afterend', newParagraph);
}

document.getElementById('btn-click').addEventListener('click', newParagraph);



// -----------------------------------------------------------------------------------------------

const contentSection = document.getElementById('content');
const wrapperDiv = document.createElement('div');
wrapperDiv.classList.add("wrapper");

const btnInput = document.createElement('button');
btnInput.id = 'btn-input';
btnInput.textContent = 'Тиц *(**,)*';
wrapperDiv.append(btnInput);

const footerElement = document.querySelector('footer');
contentSection.append(wrapperDiv, footerElement);

btnInput.addEventListener('click', function () {

    const inputWrapper = document.createElement('div');
    inputWrapper.classList.add("inputWrapper");

    const newInput = document.createElement('input');
    newInput.classList.add("input");

    newInput.type = 'text';
    newInput.placeholder = 'Напиши що небудь *(**,)*';
    newInput.name = 'inputName';

    inputWrapper.append(newInput);

    const deleteInputBtn = document.createElement('button');
    deleteInputBtn.classList.add("deleteInputButton");
    deleteInputBtn.textContent = 'X';






    

    deleteInputBtn.addEventListener('click', function () {
        inputWrapper.remove();
    });

    inputWrapper.append(deleteInputBtn);
    wrapperDiv.append(inputWrapper);
});



