/* 
Теоретичні питання
1. Які способи JavaScript можна використовувати для створення та додавання нових DOM-елементів?
2. Опишіть покроково процес видалення одного елементу (умовно клас "navigation") зі сторінки.
3. Які є методи для вставки DOM-елементів перед/після іншого DOM-елемента?

-------------------------------------------------------------------------------------------------


1.Які способи JavaScript можна використовувати для створення та додавання нових DOM-елементів?

document.createElement(): Цей метод створює новий елемент DOM з заданим ім'ям тегу. 
Ви можете налаштувати атрибути та вміст цього елемента перед додаванням його до сторінки.

element.innerHTML: Встановлює або отримує HTML або XML вміст внутрішнього HTML елемента.
Цей спосіб може бути використаний для вставки великих фрагментів HTML одночасно.

element.insertAdjacentHTML(): Цей метод вставляє вказаний текст HTML або вузол під час виконання 
вказаної HTML-розмітки відносно визначеного елемента та вказаної позиції відносно цього елемента.

document.createTextNode(): Цей метод створює новий текстовий вузол з вказаним текстом.
Ви можете додати цей вузол до будь-якого елемента.

element.cloneNode(): Цей метод створює копію вказаного вузла з включенням або виключенням дочірніх вузлів, 
 але без обробки подій клонованих елементів.
Ви можете використовувати його для створення копії вже існуючого елемента та його додавання до сторінки.




2.Опишіть покроково процес видалення одного елементу (умовно клас "navigation") зі сторінки.

Отримання посилання на елементи з класом "navigation"
const navigationElements = document.querySelectorAll(".navigation");

Вибір елемента для видалення (наприклад, перший елемент з колекції)
const elementToRemove = navigationElements[0];

Видалення елемента зі сторінки
elementToRemove.parentNode.removeChild(elementToRemove);



3.Які є методи для вставки DOM-елементів перед/після іншого DOM-елемента?

insertBefore(): Цей метод вставляє вказаний елемент перед вказаним елементом,
який є дочірнім вказаного батьківського елемента.
parentElement - батьківський елемент, перед яким потрібно вставити новий елемент.
newElement - новий елемент, який потрібно вставити.
referenceElement - елемент, перед яким потрібно вставити новий елемент.
insertAdjacentHTML(): Цей метод вставляє вказаний HTML перед або після вказаного елемента. 
targetElement - елемент, перед або після якого потрібно вставити HTML.
position - позиція вставки. Може бути одним із значень: 'beforebegin', 'afterbegin', 'beforeend' або 'afterend'.
htmlString - рядок HTML, який потрібно вставити.
*/

// -------------------------------------------------------------------------------------------------------------------

// Практичні завдання
//  1. Створіть новий елемент <a>, задайте йому текст "Learn More" і атрибут href з посиланням на "#". Додайте цей елемент в footer після параграфу.


const learnMoreLink = document.createElement("a");
learnMoreLink.classList.add("footer__link")
learnMoreLink.textContent = "Learn More";
learnMoreLink.setAttribute("href", "#");
const footer = document.querySelector("footer");
const paragraph = footer.querySelector("p");
footer.insertBefore(learnMoreLink, paragraph.nextSibling);




//  2. Створіть новий елемент <select>. Задайте йому ідентифікатор "rating",
//  і додайте його в тег main перед секцією "Features".
//   Створіть новий елемент <option> зі значенням "4" і текстом "4 Stars",
//  і додайте його до списку вибору рейтингу.
//   Створіть новий елемент <option> зі значенням "3" і текстом "3 Stars",
//  і додайте його до списку вибору рейтингу.
//   Створіть новий елемент <option> зі значенням "2" і текстом "2 Stars",
//  і додайте його до списку вибору рейтингу.
//   Створіть новий елемент <option> зі значенням "1" і текстом "1 Star",
//  і додайте його до списку вибору рейтингу.




const mainElement = document.querySelector("main");
const featuresSection = mainElement.querySelector("#features");
const selectElement = document.createElement("select");
selectElement.id = "rating";

const options = [
  { value: "4", text: "4 Stars" },
  { value: "3", text: "3 Stars" },
  { value: "2", text: "2 Stars" },
  { value: "1", text: "1 Star" }
];

for (let i = 0; i < options.length; i++) {
  const option = document.createElement("option");
  option.classList.add("select__option");
  option.value = options[i].value;
  option.textContent = options[i].text;
  selectElement.appendChild(option);
}

mainElement.insertBefore(selectElement, featuresSection);










// const selectElement = document.createElement("select");
// selectElement.id = "rating";

// const option4 = document.createElement("option");
// option4.classList.add("select__option")
// option4.value = "4";
// option4.textContent = "4 Stars";

// const option3 = document.createElement("option");
// option3.classList.add("select__option")
// option3.value = "3";
// option3.textContent = "3 Stars";

// const option2 = document.createElement("option");
// option2.classList.add("select__option")
// option2.value = "2";
// option2.textContent = "2 Stars";

// const option1 = document.createElement("option");
// option1.classList.add("select__option")
// option1.value = "1";
// option1.textContent = "1 Star";

// selectElement.appendChild(option4);
// selectElement.appendChild(option3);
// selectElement.appendChild(option2);
// selectElement.appendChild(option1);

// const mainElement = document.querySelector("main");
// const featuresSection = mainElement.querySelector("#features");
// mainElement.insertBefore(selectElement, featuresSection);